---
title: Now
description: "What I've been up to"
layout: layouts/now.njk
date: 2023-10-11
---

I am married!

I am co-directing the [Timberwolves PACKcussion drumline](https://www.nba.com/timberwolves/entertainment/packcussion). Come see us at a game!

I updated this site to use the [Flexoki color scheme by Steph Ango](https://stephango.com/flexoki), the [Haskoy font by @ertekinno](https://github.com/ertekinno/haskoy), and the [Brutalist Mono font](https://github.com/BRUTALISM/Brutalist).
