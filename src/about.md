---
layout: layouts/post.njk
title: About this website
description: About tonymottaz.com
---

Hi! I'm Tony. I am a front end web developer based in Minnesota.

This is my personal website. I write [blog posts](/blog) occasionally, and sometimes include some
other stuff, too.

## Colophon

I use [11ty](https://www.11ty.dev/) to develop and build this website. It is deployed with [Netlify](https://www.netlify.com/). I do not have access to or set any tracking, analytics, cookies, or page view counting. I leave [Netlify's site analytics feature](https://docs.netlify.com/monitor-sites/site-analytics/) disabled at all times.

The source code can be found in [this Codeberg repository](https://codeberg.org/tonymottaz/tonymottaz.com).

The primary color scheme is based on [Flexoki](https://stephango.com/flexoki). Code blocks use the [a11y-dark theme by Eric Bailey](https://github.com/ericwbailey/a11y-syntax-highlighting).

I use [system fonts](https://modernfontstacks.com/) on this website to make it lean and fast.

## Site accessibility

I am committed to making this website accessible to anyone who wishes to view it. If you find any issues, please [email me](mailto:issues@tonymottaz.com) at **issues@tonymottaz.com**. I take these issues very seriously.
