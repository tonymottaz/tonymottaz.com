---
title: How to tune a marching snare drum
description: In this blog post, I talk through my process for tuning marching snare drums.
date: 2023-04-13
---

I have tuned a lot of marching snare drums over the years. I think I have found a system for tuning them that is practical and sounds great. This post describes that system.

Disclaimer: This system works for _me_ and achieves a sound that _I prefer_. I do not claim that this is the "best way" or the "best sound". Your mileage may vary.

## General principles

Before we get into the details, I want to describe a few general principles that I follow:

1. **Use your ears** — I don't pay much attention to tools that claim to measure the tension of the head or the tension of the tension rods. I also don't attempt to count the number of turns I apply to each tension rod. At the end of the day, I want a particular sound. I trust my ears to lead me there.
2. **Minimize the accessories** — I believe that you can get a great sounding drum without adding other materials. I avoid adding tape, cloth, or other accessories that augment the sound.
3. **Experiment** — If the drum is not sounding right to me, I am willing to throw out what I think is the "correct" way to tune the drum and experiment. Different situations and environments require different approaches. Yes, this even means I will try adding tape or patches in certain scenarios.

## Changing the heads

If you can, set the drum down on carpet or a blanket so that there is evenly-applied damping to the head you aren't dealing with. When seating a new head, it's easiest if you cannot hear the other head.

Grease the lug bolts using white lithium grease. You need less grease than you think. A small dot on the end is plenty. This step is not critical if the lugs have been well-greased over time.

Start with the top head. Put it on, and slowly start to apply tension using a crisscross pattern around the tension rods.

{% image "img/tension_pattern.jpeg" "A drawing of a circle representing a drum and twelve dots representing the twelve lugs on a drum. Each lug has a number and arrows between them to indicate the order in which you should tension the lugs. If the lugs were numbered like a clock, you would go in the order: 1, 7, 6, 12, 3, 9, 10, 4, 2, 8, 5, 11." %}
This is the tension pattern that I like to use. I see the lugs in three groupings of four: a vertical group, a horizontal group, and the four corners.
{% endimage %}

At the beginning, you can use a couple full turns of your drum key. After a couple passes it's best to use half turns. Once you get to about 70% of the final tension, I wouldn't go more than a quarter or eighth of a turn at a time.

As you are tensioning the head, tap on it with a drum stick next to the lugs and listen. You will hear several tones and overtones. You want to try to find the fundamental pitch. This will take time and experience. Listen for the tone that increases in pitch as you increase the tension. Overtones might decrease pitch or change in volume instead.

Use the fundamental tone to adjust the amount of tension you add to each lug. Do not count the number of turns you apply to each tensioning rod. It may seem like this should keep the head in tune as you go, but in my experience it is less reliable than using your ear.

After every lap around the head, do a brief check for any lugs that are significantly off pitch. Bring the head closer to in-tune by adding tension to the low-pitched lugs. It doesn't need to be perfect at this stage, just close enough that the head seats properly and you don't warp the rim.

Shoot for getting to about 80% of the final tension in the head. Once you're there, take the time to really tune it as perfectly as you can. The goal is to reduce the amount of overtones you hear when you tap anywhere on the head. Then flip it and change the bottom head.

As you are bringing up the bottom head on a snare drum, be aware that the shell has a dip where the snares go. This causes the fundamental tone to be a lower pitch on those lugs. That's okay — leave it like that. You will fix that once you are close to the final pitch. Instead, when you check the tuning after each lap, make sure that opposite lugs are the same pitch. The lugs at the dip will be lower pitched than the lugs 90 degrees off. Bring the bottom head to 80% tension as well.

If the drum head material is a sheet of plastic or mylar, then it's a good idea at this stage to apply pressure with the heel of your hand to stretch and seat the head. In my opinion, woven heads that are typically used on snare drums don't need this at all.

## Tuning the heads

Once you have both heads at 80% tension, put the drum on a stand, bottom head up. You're going to bring it to 100% of the final tension slowly, keeping it in tune along the way. At this stage, apply more tension to the low lugs around the snare beds so that it starts to even out in pitch. When you reach 100% tension, you want the drum to be fully in tune.

You know you've reached 100% tension when the you get a nice "ping" sound when you tap it with your stick halfway between the center and edge of the drum. It's important to be fully in tune in order to hear that. You know that you have gone too far if the overall pitch of the head stops rising as you apply more tension. You are at risk of breaking the drum head at this point.

Finally, flip the drum over and start bringing up the top head. This will drop in pitch significantly over the next several hours and days. Don't bring it up to 100% right away. Bring it up to 90%, and continue to bring it back up to 90% over the next several hours and days.

Every time you add tension to the top head, the sound of the drum becomes more "lively". This will last for a day or two. For this reason, I usually save 100% tension for show day.

You know that you've gone too high if you start to get a hollow or tenor-ish sound from the drum. It is *always* better to leave the top head too low than to go too high.

## Adjusting the snares

### Tuning the snares

Turn the snares off and use a pencil to create a bridge, like a violin. Tighten the snares until you can hear a clear pitch from each strand. Using a screwdriver, adjust each strand so that they are all fairly close to the same pitch. It doesn't need to be exact — in fact, I think it's better if they're *slightly* different from each other. The important thing is not to have any that are significantly different from the others, or else you might get an unpleasant buzzing sound.

### Level the snare bed

Turn the snares on and adjust the heights on both sides so that the snares are fully off the head, not touching anything. You'll notice that the bottom head has a slight dome shape to it instead of being flat. This means that as you bring the snares down, they will touch the middle of the head before the bearing edge.

Slowly bring the snares down toward the head. Adjust both sides at the same time so that it comes down level. Keep going until they are *just* starting to apply pressure to the bearing edge.

Note that this step can often be thwarted by snares that have been bent. It is a common error to bend the snares too far over the drum, creating a crease. If this happens, you will need to experiment. I would try to set it up so that as much of the snares are touching the head as possible.

Another thing to look out for — the hardware that holds the snares can sometimes have a lot of play in it, wiggling around and changing how the snares lay on the drum head. When this happens, you will need to check that frequently to put it back in a neutral position.

### Adjust the tension on the snares

To find the perfect tension on the snares, set the drum up with the top head up. Loosen the snares way too loose so that you don't even get a snare response. While hitting the center of the top head softly, slowly tighten the snares. Listen closely to how the snare response changes. In general, you will go through the following phases:

1. No snare response
2. "Wet" and "rattly"
3. Buzzy
4. Just right
5. Choked, metallic

## One final note

As I said before, this method helps me tune to my preferred snare drum sound. The sound is crisp, bright, and exposed. If you need to "hide the dirt", then you will need to make some adjustments. I am not going to go into what those are — you should experiment and discover it for yourself!
